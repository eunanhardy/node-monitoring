#!/usr/bin/python
import sys

def process_file(file_path):

    file_array = []
    for file in open(file_path,"r"):
        try:
            line_data = file.split(" ")
            data = {"monitor_timestamp":line_data[0],"node_timestamp":line_data[1],"node_name":line_data[2],"status":line_data[3]}
            if data["status"] == "LOST" or data["status"] == "FOUND":
                data.update({"ext_node":line_data[4]})
        
            file_array.append(data)
        except IndexError:
                print "Error parsing file"
                sys.exit(0)

    return file_array


def main():
    try:
        fileData = process_file(sys.argv[1])
    except IndexError:
        print "ARGUMENT NOT FOUND: You probably forgot to pass in the file arguement, Please Try Again"
        sys.exit(0)    

    node_array = []

    for index,data in enumerate(fileData):

        if data["status"] == "HELLO":


            index_d = 0
            if not any(node_d['node_name'] == data["node_name"] for node_d in node_array):
                index_d+=1
                if "ext_node" in data:
                    node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                    node_array.append(node)
                else:
                    node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"int_node":data["node_name"]}
                    node_array.append(node) 

            else:
                index_d+=1
                del node_array[index_d]
                if "ext_node" in data:
                    node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                    node_array.append(node)
                else:
                    node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"int_node":data["node_name"]}
                    node_array.append(node) 




        if data["status"] == "FOUND":

            
            index_d = 0
            if not any(node_d['node_name'] == data['ext_node'] for node_d in node_array):
                index_d+=1
                
                node = {"node_name":data["ext_node"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                node_array.append(node)

            elif any(node_d['node_name'] == data['node_name'] for node_d in node_array):
                index_d+=1
                del node_array[index_d]
                node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data["node_name"]}
                node_array.append(node)
                
                '''
                if "ext_node" in data:
                    node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data["node_name"]}
                    node_array.append(node)
                else:
                    node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"int_node":data["node_name"]}
                    node_array.append(node)
                '''
            

            
            index_d = 0
            if not any(node_d['node_name'] == data['ext_node'] for node_d in node_array):
                index_d+=1

                node = {"node_name":data["ext_node"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                node_array.append(node)
            elif any(node_d['node_name'] == data['ext_node'] for node_d in node_array):
                index_d+=1
                del node_array[index_d]
                node = {"node_name":data["ext_node"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                node_array.append(node)    

        if data["status"] == "LOST":
            
            index_d = 0
            if not any(data['node_name'] == node_d['node_name'] for node_d in node_array):

                index_d+=1
                node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                node_array.append(node)
            else:
                index+=1
                del node_array[index_d]
                node = {"node_name":data["node_name"],"timestamp":data["monitor_timestamp"],"node_status":"ALIVE","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                node_array.append(node)

            index_d = 0

            if not any(data["ext_node"] == node_d["node_name"] for node_d in node_array):

                index_d+=1
                node = {"node_name":data["ext_node"],"timestamp":data["monitor_timestamp"],"node_status":"DEAD","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                node_array.append(node)

            else:

                index_d+=1
                del node_array[index_d]
                node = {"node_name":data["ext_node"],"timestamp":data["monitor_timestamp"],"node_status":"DEAD","status":data["status"],"ext_node":data["ext_node"],"int_node":data['node_name']}
                node_array.append(node)







                        




                
                    
    for node in node_array:
        
        
        if "ext_node" not in node:

            print node["node_name"] + " " + node["node_status"] + " " + node["timestamp"] + " " + node["int_node"] + " " + node["status"]

        else:
            print node["node_name"] + " " + node["node_status"] + " " + node["timestamp"] + " " + node["int_node"] + " " + node["status"] + " " + node["ext_node"]    

    
if __name__ == "__main__":


    main()
